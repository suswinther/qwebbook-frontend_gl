import Vue from 'vue'
import Vuetify, {
  VApp,
  VAlert,
  VAutocomplete,
  VBtn,
  VBtnToggle,
  VCard,
  VCardText,
  VCardTitle,
  VCheckbox,
  VLayout
} from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  components: {
    VApp,
    VAlert,
    VAutocomplete,
    VBtn,
    VBtnToggle,
    VCard,
    VCardText,
    VCardTitle,
    VCheckbox,
    VLayout
  }
})
