import Vue from 'vue'
import Router from 'vue-router'
import Booking from '../views/Booking'
import Confirmation from '../views/Confirmation'
import Appointment from '../views/Appointment'
import CancelAppointment from '../views/CancelAppointment'
import RescheduleAppointment from '../views/RescheduleAppointment'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'booking',
      component: Booking,
      props: true
    },
    {
      path: '/confirmation',
      name: 'confirmation',
      component: Confirmation,
      props: true
    },
    {
      path: '/:appointmentId',
      name: 'appointment',
      component: Appointment,
      children: [{
        path: 'cancel',
        name: 'cancel',
        component: CancelAppointment
      }]
    },
    {
      path: '/:appointmentId/reschedule',
      name: 'reschedule',
      component: RescheduleAppointment,
      props: true
    }
  ]
})
