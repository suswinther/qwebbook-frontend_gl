export default {
  name: 'DataService',
  methods: {
    convertObject: function (object) {
      /* Since boolean values in the configuration are strings, we need to convert them */
      var newObject = {}
      for (var key in object) {
        var value = object[key]
        if (value !== '') {
          if (value === 'true' || value === 'false') {
            newObject[key] = JSON.parse(value)
          } else if (key === 'qwebbookLanguages') {
            newObject[key] = value.split(',')
          } else if (key === 'uiFields') {
            var fields = JSON.parse(value)
            newObject[key] = {}
            for (var fieldKey in fields) {
              // Only push those fields that are actually enabled
              if (fields[fieldKey]['s'] === 1) {
                newObject[key][fieldKey] = fields[fieldKey]
                newObject[key][fieldKey].name = fieldKey
              }
            }
          } else {
            newObject[key] = value
          }
        }
      }
      return newObject
    },
    getByValue: function (array, value) {
      for (var i = 0; i < array.length; i++) {
        if (array[i].publicId === value) {
          return i
        }
      }
    }
  }
}
