import axios from 'axios'
axios.defaults.baseURL = '/qwebbook/rest/schedule'
axios.defaults.headers.post['Content-Type'] = 'application/json'
export default {
  name: 'RestService',
  methods: {
    getUrl: function (baseUrl, services) {
      var url = baseUrl
      for (var j = 0; j < services.length; j++) {
        var serviceId = services[j].publicId
        url = url + ';servicePublicId=' + serviceId
      }
      return url
    },
    getMessages: function (language) {
      return axios.get('/uiMessages?lang=' + language)
    },
    getConfiguration: function () {
      return axios.get('/configuration')
    },
    getServices: function () {
      return axios.get('/services')
    },
    getBranchGroups: function (selectedServices) {
      var baseUrl = '/branchGroups'
      var url = this.getUrl(baseUrl, selectedServices)
      return axios.get(url)
    },
    getAvailableDates: function (selectedBranch, selectedServices, customSlotLength) {
      var baseUrl = '/branches/' + selectedBranch.id + '/dates'
      var url = this.getUrl(baseUrl, selectedServices)
      url = url + ';customSlotLength=' + customSlotLength
      return axios.get(url)
    },
    getAvailableTimes: function (date, selectedServices, customSlotLength, selectedBranch) {
      var baseUrl = '/branches/' + selectedBranch.id + '/dates/' + date + '/times'
      var url = this.getUrl(baseUrl, selectedServices)
      url = url + ';customSlotLength=' + customSlotLength
      return axios.get(url)
    },
    reserveTimeSlot: function (selectedBranch, date, time, customSlotLength, selectedServices) {
      var data = {}
      data.services = []
      for (var i = 0; i < selectedServices.length; i++) {
        var service = {}
        service.publicId = selectedServices[i].publicId
        data.services.push(service)
      }
      var json = JSON.stringify(data)
      var url = '/branches/' + selectedBranch.id + '/dates/' + date + '/times/' + time + '/reserve;customSlotLength=' + customSlotLength
      return axios.post(url, json)
    },
    sendVerificationCode: function (phoneNumber) {
      var data = {}
      var json
      data.phoneNumber = phoneNumber
      json = JSON.stringify(data)
      return axios.post('/sendSMS', json)
    },
    verifyPhoneNumber: function (code, phoneNumber) {
      var data = {}
      var json
      data.phoneNumber = phoneNumber
      data.message = code.toString()
      json = JSON.stringify(data)
      return axios.post('/verifySms', json)
    },
    confirmAppointment: function (selectedServices, reservedAppointmedId, contactDetails, languageCode, recaptchaToken, totalCost, customSlotLength) {
      var url = '/appointments/' + reservedAppointmedId + '/confirm'
      var data = {
        customer: {
          firstName: '',
          lastName: '',
          dateOfBirth: '',
          email: '',
          phone: ''
        },
        languageCode: '',
        notificationType: '',
        captcha: '',
        custom: {
          peopleServices: [],
          totalCost: 0
        }
      }
      for (var i = 0; i < selectedServices.length; i++) {
        var service = selectedServices[i]
        var customObject = {}
        customObject.publicId = service.publicId
        customObject.qpId = service.qpId
        customObject.adult = service.adult
        customObject.name = service.name
        customObject.child = service.child
        data.custom.peopleServices.push(customObject)
      }
      data.custom.totalCost = totalCost
      data.custom.customSlotLength = customSlotLength
      data.customer.email = contactDetails.Email
      data.customer.phone = contactDetails.formattedPhone
      data.languageCode = languageCode
      data.custom = JSON.stringify(data.custom)
      data.captcha = recaptchaToken
      var json = JSON.stringify(data)
      return axios.post(url, json)
    },
    getAllCountries: function () {
      return axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;callingCodes')
    },
    getCountry: function (code) {
      return axios.get('https://restcountries.eu/rest/v2/alpha/' + code + '?fields=name;alpha2Code;callingCodes')
    },
    getCountryByLangCode: function (code) {
      return axios.get('https://restcountries.eu/rest/v2/alpha/' + code[1] + '?lang/' + code[0])
    },
    checkExistingAppointments: function (phoneNumber) {
      return axios.get('/appointments/phone/' + phoneNumber)
    },
    getAppointment: function (appointmentId) {
      return axios.get('/appointments/qwebbook/' + appointmentId)
    },
    deleteAppointment: function (appointmentId) {
      return axios.delete('/appointments/' + appointmentId)
    },
    releaseTimeslot: function (appointmentId) {
      return axios.delete('/appointments/reserved/' + appointmentId)
    },
    rescheduleAppointment: function (appointment, recaptchaToken, time, date) {
      var data = {}
      data.captcha = recaptchaToken
      data.time = time
      data.date = date
      data.custom = {}
      data.custom.peopleServices = appointment.peopleServices
      data.custom.totalCost = appointment.totalCost
      data.custom.customSlotLength = appointment.customSlotLength
      data.customer = appointment.customer
      data.services = appointment.services
      data.publicBranchId = appointment.publicBranchId
      data.languageCode = appointment.languageCode
      data.externalId = appointment.externalId
      data.custom = JSON.stringify(data.custom)
      var json = JSON.stringify(data)
      return axios.post('/appointments/' + appointment.publicId + '/reschedule', json)
    }
  }
}
