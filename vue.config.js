module.exports = {
  devServer: {
    proxy: 'http://localhost:8090'
  },
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/qwebbook/'
    : '/'
}
